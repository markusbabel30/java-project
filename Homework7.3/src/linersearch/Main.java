package linersearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		Random random = new Random();

		System.out.print("Enter array size: ");

		int arraySize = sc.nextInt();

		System.out.println();

		int[] array = new int[arraySize];
		for (int i = 0; i < array.length; i++) {
			array[i] = random.nextInt(10);
		}

		System.out.println("What number are we searching for in the array?");
		int element = sc.nextInt();
		System.out.println("The array is: ");
		System.out.println(Arrays.toString(array));
		System.out.println();

		if (linerSearch(array, element) == -1) {
			System.out.println("There is no such number in the array");
		} else {
			System.out.println("Index of number " + element + " in the array is: " + linerSearch(array, element));
		}
	}

	public static int linerSearch(int[] array, int element) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == element) {
				return i;
			}
		}
		return -1;
	}
}
