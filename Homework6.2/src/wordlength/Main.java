package wordlength;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the text");
		String text = sc.nextLine();

		String[] arr = text.split(" ");
		String longestWord = "";
		String similarWords = "";
		String check = "";

		for (int i = 0; i < arr.length; i++) {
			if (arr[i].length() > longestWord.length()) {
				longestWord = arr[i];
			} else if (arr[i].length() == longestWord.length()) {

				if (arr[i].length() == check.length()) {
					similarWords += ", " + arr[i];

				} else {
					similarWords = longestWord + ", " + arr[i];
					check = arr[i];
				}

			}
		}

		if (check.length() >= longestWord.length()) {
			System.out.println("There are several words of the same length in this line: " + similarWords + " ");
		} else
			System.out.println("The longest word is: " + longestWord);

	}

}
