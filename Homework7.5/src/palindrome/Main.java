package palindrome;

public class Main {

	public static void main(String[] args) {
		int max = 0;
		int var;
		int x = 0;
		int y = 0;
		for (int i = 100; i <= 999; i++) {
			for (int j = 100; j <= 999; j++) {
				var = i * j;
				if (isPalindrome(var) && (var > max)) {
					max = var;
					x = i;
					y = j;
				}
			}

		}
		System.out.println("The largest palindrome obtained by multiplying two three-digit numbers is: ");
		System.out.println(x + " * "+ y + " = " + max);
	}

	public static boolean isPalindrome(int num) {
		int num1;
		int num2;
		int num3;
		int num4;
		int num5;
		int num6;

		num1 = num / 100000;
		num2 = num % 100000 / 10000;
		num3 = num % 10000 / 1000;
		num4 = num % 1000 / 100;
		num5 = num % 100 / 10;
		num6 = num % 10;

		if (num1 == num6 && num2 == num5 && num3 == num4) {
			return true;
		} else {
			return false;
		}

	}
}