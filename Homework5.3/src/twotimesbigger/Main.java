package twotimesbigger;

import java.util.Arrays;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] arrayOne = new int[15];

		Random rn = new Random();

		for (int i = 0; i < arrayOne.length; i++) {
			arrayOne[i] = rn.nextInt(99);
		}

		int[] arrayTwo = Arrays.copyOfRange(arrayOne, 0, arrayOne.length * 2);
		for (int i = arrayOne.length; i < arrayTwo.length; i++) {
			arrayTwo[i] = arrayOne[i - arrayOne.length] * 2;
		}

		System.out.println(Arrays.toString(arrayOne));
		System.out.println(Arrays.toString(arrayTwo));
	}
}