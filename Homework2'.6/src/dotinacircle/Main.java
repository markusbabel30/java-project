package dotinacircle;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		double r =4;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter x coordinate:");
		double x = sc.nextDouble();
		System.out.println("Enter y coordinate:");
		double y = sc.nextDouble();
		
		if ((Math.pow(x, 2)+Math.pow(y, 2)) <= Math.pow(r, 2)){
			System.out.println("The dot is in the circle");
		}else {
			System.out.println("The dot is not in the circle");
		}
	}
}