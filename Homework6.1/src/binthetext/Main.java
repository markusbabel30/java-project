package binthetext;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		int count = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Input text");
		String text = sc.nextLine();

		char[] arrayText = text.toCharArray();
		for (int i = 0; i < arrayText.length; i++) {
			if (arrayText[i] == 'b') {
				count++;
			}

		}
		System.out.println("in your text the character b occurs " + count + " times");
	}
}