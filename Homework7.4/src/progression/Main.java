package progression;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Input progression length (at least 3 elements): ");
		int length;
		for (;;) {
			length = sc.nextInt();
			if (length < 3) {
				System.out.println("Invalid input, try again");
			} else {
				break;
			}
		}

		int[] row = new int[length];

		System.out.println("Input progression elements : ");

		for (int i = 0; i < row.length; i++) {
			row[i] = sc.nextInt();
		}

		int result = calculateNextElement(row);

		if (result == row[0]) {
			System.out.println("Unknown type of progression or invalid progression");
		} else {
			System.out.println("The next progression element is: " + result);
		}
	}

	public static int calculateNextElement(int[] row) {

		int dif = row[1] - row[0];
		if (isArithmetic(row, dif)) {
			return row[row.length - 1] + dif;
		}

		try {
			int quot = row[1] / row[0];
			if (isGeometric(row, quot)) {
				return row[row.length - 1] * quot;
			}
		} catch (ArithmeticException e) {
		}

		double sqrt = Math.sqrt(row[0]);
		if (isSquared(row, sqrt)) {
			return row[row.length - 1] + (row[row.length - 1] - row[row.length - 2]) + 2;
		}

		double cbrt = Math.cbrt(row[row.length - 1]);
		if (isCubed(row, cbrt)) {
			return (int) (row[row.length - 1] + (row[row.length - 1] - row[row.length - 2]) + 6 * cbrt);
		}

		return row[0];
	}

	public static boolean isArithmetic(int[] row, int dif) {
		for (int i = 0; i < row.length - 1; i++) {
			if (row[i] + dif != row[i + 1]) {
				return false;
			}
		}
		return true;
	}

	public static boolean isGeometric(int[] row, double quot) {
		for (int i = 0; i < row.length - 1; i++) {
			if (row[i] * quot != row[i + 1]) {
				return false;
			}
		}
		return true;
	}

	public static boolean isSquared(int[] row, double sqrt) {
		if (sqrt % 1 != 0) {
			return false;
		}
		for (int i = 0; i < row.length - 2; i++) {
			if (row[i + 1] - row[i] + 2 != row[i + 2] - row[i + 1]) {
				return false;
			}
		}
		return true;
	}

	public static boolean isCubed(int[] row, double cbrt) {
		if (cbrt % 1 != 0) {
			return false;
		}
		if (cbrt > 0) {
			for (int i = 0; i < row.length - 2; i++) {
				if ((row[i + 1] - row[i]) + 6 * (cbrt - row.length + 2 + i) != row[i + 2] - row[i + 1]) {
					return false;
				}
			}
		} else {
			for (int i = 0; i < row.length - 2; i++) {
				if ((row[i + 1] - row[i]) - 6 * (-cbrt - row.length + 2 + i) != row[i + 2] - row[i + 1]) {
					return false;
				}
			}
		}
		return true;
	}
}