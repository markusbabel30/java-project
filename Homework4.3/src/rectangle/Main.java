package rectangle;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter rectangle height: ");
		int a = sc.nextInt();
		System.out.println("Enter rectangle width: ");
		int b = sc.nextInt();
		for (int i = 0; i < a; i++) {
			for (int j = 0; j < b; j++) {
				if (j == 0 || j == b - 1 || i == 0 || i == a - 1) {
					System.out.print("*");
				} else
					System.out.print(" ");
			}
			System.out.println();

		}

	}

}
