package files;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		File file1 = new File("new file.txt");

		try {
			file1.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		File folder1 = new File("AAAAAAAA");
		folder1.mkdirs();

		File file2 = new File(folder1, "b.docx");
		try {
			file2.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		file1.delete();
		file2.delete();
		folder1.delete();

		File workFolder = new File(".");
		File[] files = workFolder.listFiles();
		for (int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			long fileSize = files[i].length();
			String fileType = "File";
			if (files[i].isDirectory()) {
				fileType = "Folder";
			}
			System.out.println(fileName + "\t" + fileSize + "\t" + fileType);
		}

		File file = new File("hello.txt");

		try (PrintWriter pw = new PrintWriter(file)) {
			pw.println("Hello world");
		} catch (IOException e) {
			e.printStackTrace();
		}

		String[] goods = new String[] { "Mars", "Kit-cat", "Snikers" };
		int[] price = new int[] { 30, 20, 30 };
		int[] count = new int[] { 30, 50, 70 };
		String del = ",";
		File file3 = new File("report.csv");
//		    saveReport(file, del, goods, price, count);

		String text = getStringFromFile(file3);
		System.out.println(text);
	}

	public static void saveReport(File file, String del, String[] g, int[] p, int[] c) {
		try (PrintWriter pw = new PrintWriter(file)) {
			for (int i = 0; i < c.length; i++) {
				pw.println(g[i] + del + p[i] + del + c[i]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String getStringFromFile(File file) {
		String text = "";
		try (Scanner sc = new Scanner(file)) {

			for (; sc.hasNextLine();) {
				text += sc.nextLine() + System.lineSeparator();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return text;
	}

}