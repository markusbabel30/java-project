package cataloglist;

import java.io.File;

public class Main {

	public static void main(String[] args) {

		File workfolder = new File(".");

		System.out.println(catalogList(workfolder));

	}
	
	public static String catalogList(File directory) {
		File[] folders = directory.listFiles();
		String text = "";

		for (int i = 0; i < folders.length; i++) {
			if (folders[i].isDirectory())
				text = text + folders[i] + System.lineSeparator();
		}
		return text;
	}
}