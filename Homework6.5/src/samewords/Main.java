package samewords;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("What was Вовочка writing? ");
		String text = sc.nextLine();
		String ltr1;
		String ltr2;
		String part = "";

		for (int i = 1;; i++) {
			ltr1 = text.substring(0, i);
			int count = 0;

			for (int j = i; j + i <= text.length(); j += i) {
				ltr2 = text.substring(j, i + j);

				if (!ltr1.equals(ltr2)) {
					count++;
				}
			}

			if (count == 0) {
				part = ltr1;
				break;
			}
		}

		System.out.println("Вовочка was writing: " + part);
	}
}