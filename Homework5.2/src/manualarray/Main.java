package manualarray;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		System.out.println("Input array length: ");
		int l = sc.nextInt();
		int[] array = new int[l];
		for (int i = 0; i < l; i++) {
			System.out.println("Enter a number in the array");
			int n = sc.nextInt();
			array[i] = n;
		}
		System.out.println(Arrays.toString(array));
	}
}