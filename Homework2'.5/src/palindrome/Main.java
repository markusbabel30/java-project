package palindrome;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		System.out.println("Input six-digit number:");

		int n = sc.nextInt();
		if (n > 999999 ^ n < 100000) {
			System.out.println("Wrong number. You should input six-digit number!");
		} else {
			int a = n / 100000;
			int b = n % 100000 / 10000;
			int c = n % 10000 / 1000;
			int d = n % 1000 / 100;
			int e = n % 100 / 10;
			int f = n % 10 / 1;
			if (a == f && b == e && c == d)
				System.out.println("Number is palindrome!!");
			else
				System.out.println("Number is not palindrome");

		}
	}
}