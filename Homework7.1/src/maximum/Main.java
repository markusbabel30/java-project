package maximum;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		Random random = new Random();

		System.out.print("Enter array size: ");

		int arraySize = sc.nextInt();

		System.out.println();

		int[] array = new int[arraySize];
		for (int i = 0; i < array.length; i++) {
			array[i] = random.nextInt(99);
		}
		int maximum = findMax(array);

		System.out.println("The array is: ");
		System.out.println(Arrays.toString(array));
		System.out.println();
		System.out.println("The array's maximum is: ");
		System.out.println(maximum);
	}

	public static int findMax(int[] array) {

		int currentMax = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] > currentMax) {
				currentMax = array[i];
			}
		}
		return currentMax;
	}
}
