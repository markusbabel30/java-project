package rectangle;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input rectangle's lenght");
		int a = sc.nextInt();
		System.out.println("Input rectangle's hight");
		int b = sc.nextInt();

		drawRectangle(a, b);

	}

	public static void drawRectangle(int a, int b) {
		for (int i = 1; i <= a; i++) {
			for (int j = 1; j <= b; j++) {
				if (j == 1 || j == b || i == 1 || i == a) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}