package money;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		String[] toTen = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven",
				"twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", };
		String[] toHundred = { "", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
		String[] toBillion = { "", "hundred", "thousand", "million" };

		Scanner sc = new Scanner(System.in);
		System.out.println("Please, enter the amount of money you have:");

		double money = sc.nextDouble();
		if (money >= 1000000000)
			System.out.println("You are too rich for this program =)");
		else if (money < 0)
			System.out.println("You cannot have negative amount of money. Try Again");
		else if (money == 0)
			System.out.println("Go find a job to earn some money");
		else {

			String temp = String.format("%.2f", money);
			String[] tempText = temp.split("[.]");

			int before = Integer.parseInt(tempText[0]);
			int after = Integer.parseInt(tempText[1]);

			int millions = (int) money / 1000000;
			int thousands = (int) (money - millions * 1000000) / 1000;
			int hundreds = (int) money - ((int) money / 1000 * 1000);

			String tR = "";
			String dollars = "";
			String cents = "";

			if (before > 0) {

				if (millions > 0) {

					if (millions / 100 > 0)
						tR = toTen[millions / 100] + " " + toBillion[1] + " ";
					if (millions % 100 > 19)
						tR = tR + toHundred[millions % 100 / 10] + " " + toTen[millions % 100 % 10];
					else
						tR = tR + toTen[millions % 100];

					tR = tR + " " + toBillion[3];

					if (millions > 1)
						tR = tR + "s ";
					else
						tR = tR + " ";
				}

				if (thousands > 0) {

					if (thousands / 100 > 0)
						tR = tR + toTen[thousands / 100] + " " + toBillion[1] + " ";
					if (thousands % 100 > 19)
						tR = tR + toHundred[thousands % 100 / 10] + " " + toTen[thousands % 100 % 10];
					else
						tR = tR + toTen[thousands % 100];

					tR = tR + " " + toBillion[2];
					if (thousands > 1)
						tR = tR + "s ";
					else
						tR = tR + " ";
				}

				if (hundreds > 0) {

					if (hundreds / 100 > 0)
						tR = tR + toTen[hundreds / 100] + " " + toBillion[1] + " ";
					if (hundreds % 100 > 19)
						tR = tR + toHundred[hundreds % 100 / 10] + " " + toTen[hundreds % 100 % 10];
					else
						tR = tR + toTen[hundreds % 100];
				}
				dollars = tR + " dollars";
			}

			if (after > 0) {
				if (after % 100 > 19)
					cents = cents + " and " + toHundred[after % 100 / 10] + " " + toTen[after % 100 % 10];
				else
					cents = cents + " and " + toTen[after % 100];

				if (after > 1)
					cents = cents + " cents";
				else
					cents = cents + " cent";
			}
			String finalResult = "You have: " + dollars + cents;
			System.out.println(finalResult);
		}
	}
}