package luckyticket;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		int a;
		int b;
		int c;
		int d;
		int n;
		int test1;
		int test2;

		System.out.println("Input 4 digits of your ticket number");
		n = sc.nextInt();

		if (n > 9999 ^ n < 1000) {
			System.out.println("Wrong number. You should input 4 digits!");
		} else {

			a = n / 1000;
			b = n % 1000 / 100;
			c = n % 1000 % 100 / 10;
			d = n % 1000 % 100 % 10;

			test1 = a + b;
			test2 = c + d;

			if (test1 >= 10) {
				a = test1 / 10;
				b = test1 % 10;
				test1 = a + b;
			}
			if (test2 >= 10) {
				c = test2 / 10;
				d = test2 % 10;
				test2 = c + d;
			}

			if (test1 == test2) {
				System.out.println("Congratulations. You found yourself a LUCKY TICKET!!!");
			} else {
				System.out.println("Sorry, this ticket isn't lucky... Better luck next time...");
			}

		}
	}
}
