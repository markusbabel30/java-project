package hourglass;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		int n;
		System.out.println("Enter hourglass width");
		n = sc.nextInt();
		if (n % 2 != 0) {
			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= n; j++) {
					if (i >= j & i >= (n + 1) - j | (i <= j & i <= (n + 1) - j)) {
						System.out.print("*");

					} else {
						System.out.print(" ");
					}
					if (j % n == 0) {
						System.out.println("");
					}
				}
			}
		} else {
			System.out.println("Wrong! You should input an odd number to receive the correct hourglass figure.");
		}
	}
}
