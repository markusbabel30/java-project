package apartmentlocation;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
	    int apartment;
	    System.out.println("Input number of the apartment: ");
	    apartment = sc.nextInt();
	    
	    int entrance = (apartment-1)/36+1;
	    int floor = (apartment-36*(entrance-1)-1)/4+1;
	   
	    if (apartment > 0 && apartment <= 180) {
	    	System.out.println("The apartment is located on the "+floor+" floor of the "+entrance+" entrance ");
		}else 
			System.out.println("The apartment is not located in this house ");
		
	}

}
