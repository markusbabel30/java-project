package dotinatriangle;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int ax = 0;
		int ay = 0;

		int bx = 4;
		int by = 4;

		int cx = 6;
		int cy = 1;

		System.out.println("Enter x coordinate:");

		double ox = sc.nextDouble();

		System.out.println("Enter y coordinate:");

		double oy = sc.nextDouble();

		double prod1 = ((bx - ax) * (oy - ay) - (by - ay) * (ox - ax));
		double prod2 = ((cx - bx) * (oy - by) - (cy - by) * (ox - bx));
		double prod3 = ((ax - cx) * (oy - cy) - (ay - cy) * (ox - cx));

		if (prod1 <= 0 && prod2 <= 0 && prod3 <= 0 || prod1 >= 0 && prod2 >= 0 && prod3 >= 0) {
			System.out.println("The dot is in the triangle");
		} else {
			System.out.println("The dot is not in the triangle");
		}
	}

}