package nickname;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Input you nickname: ");
		String nickName = sc.nextLine();
		String tempNickName = nickName.toLowerCase();

		char[] symbols = tempNickName.toCharArray();
		int correct = 0;

		if (symbols.length <= 4) {
			correct = 3;
		} else if (!(symbols[0] >= 'a' && symbols[0] <= 'z')) {
			correct = 4;
		} else {
			for (int i = 0; i < symbols.length; i++) {
				char s = symbols[i];
				if (!((s >= 'a' && s <= 'z') || (s == '_') || (s >= '0' && s <= '9'))) {
					correct = 1;
					break;
				}
			}
		}
		if (correct == 0) {
			System.out.println("Nickname " + nickName + " correct");
		} else {
			System.out.println("Nickname " + nickName + " not correct");
		}

	}

}