package turnbydegree;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Random random = new Random();

		System.out.print("Enter array size: ");
		
		int arraySize = sc.nextInt();
		
		System.out.println();
		
		int[][] array = new int[arraySize][arraySize];

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = random.nextInt(10);
			}
		}
		
		System.out.println("Original array is: ");
		Arrays.stream(array).map(Arrays::toString).forEach(System.out::println);
		System.out.println();
		System.out.println("Enter how many degrees do you want to rotate this array: ");
		System.out.println("90, 180 or 270?");
		
		int deg = sc.nextInt();
		int rotate = 0;

		if (deg == 90 || deg == 180 || deg == 270) {
			switch (deg) {
			case 90 -> rotate = 1;
			case 180 -> rotate = 2;
			case 270 -> rotate = 3;
			}

			int len = array.length - 1;

			for (int a = 1; a <= rotate; a++) {
				for (int i = 0; i < len - i; i++) {
					for (int j = i; j < len - i; j++) {
						int temp = array[i][j];
						array[i][j] = array[len - j][i];
						array[len - j][i] = array[len - i][len - j];
						array[len - i][len - j] = array[j][len - i];
						array[j][len - i] = temp;
					}
				}
			}
			System.out.println();
			Arrays.stream(array).map(Arrays::toString).forEach(System.out::println);
		} else {
			System.out.println("Wrong input! Please try again!");
		}
	}
}