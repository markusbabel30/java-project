package sample;

import java.util.Scanner;

public class Main {

  public static void main(String[] args) {
    // TODO Auto-generated method stub
    Scanner sc = new Scanner(System.in);
    System.out.println("Input you nickname: ");
    String codeLine = sc.nextLine();
    int bracket = 0;
    char[] symbols = codeLine.toCharArray();
    for (int i = 0; i < symbols.length; i++) {
      if (symbols[i] == '{') {
        bracket += 1;
      }
      if (symbols[i] == '}') {
        bracket -= 1;
      }
    }
    if (bracket == 0) {
      System.out.println("code correct");
    } else {
      System.out.println("code incorrect");
    }
  }

}