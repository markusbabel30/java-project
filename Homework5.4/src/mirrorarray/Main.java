package mirrorarray;

import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Random rn = new Random();
		System.out.println("Enter length of the massive:");

		int l = sc.nextInt();
		int[] a = new int[l];
		for (int i = 0; i < a.length; i++)
			a[i] = rn.nextInt(100);

		System.out.println(Arrays.toString(a));

		int start = 0;
		for (int i = 0, j = a.length - 1; i <= j / 2; i++) {
			int temp = a[i];
			a[i] = a[j + start - i];
			a[j + start - i] = temp;
		}

		System.out.println(Arrays.toString(a));
	}
}