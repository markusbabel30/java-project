package factorial;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number between 4 and 16:");
		int a = sc.nextInt();
		long f = 1;

		while (a < 4 || a > 16) {
			System.out.println("Wrong number");
			a = sc.nextInt();
		}

		for (int i = a; i != 0; i--) {
			f *= i;
		}
		System.out.println("Factorial " + a + " = " + f);

	}

}
